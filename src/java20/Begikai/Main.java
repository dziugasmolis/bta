package java20.Begikai;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String duomenys = new File("").getAbsolutePath()
                + "/src/java20/Begikai/duomenys.txt";
        String rez = new File("").getAbsolutePath()
                + "/src/java20/Begikai/rezultatai.txt";
        Map<Integer, Begikas> begikai = skaityti(duomenys);
        List<Begikas> finisave = rastiFinisavusius(begikai);
        System.out.println(finisave);
        Collections.sort(finisave, (o1, o2) ->
                o1.getSkirtumas().compareTo(o2.getSkirtumas()));
        spausdinti(finisave, rez);
    }

    public static void spausdinti(List<Begikas> begikai, String rezFailas) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(rezFailas))) {
            output.write(begikai.get(0).getVardas() + " " + begikai.get(0).getPavarde() +
                    " I vieta\n");
            for(Begikas obj: begikai) {
                output.write(obj.getVardas() + " " + obj.getPavarde() + " " +
                        obj.getPunktai().size() + " " + obj.getSkirtumas()+"\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Begikas> rastiFinisavusius(Map<Integer, Begikas> begikai) {
        List<Begikas> finisave = new ArrayList<>();
        for (Begikas obj: begikai.values()) {
            if(obj.getPunktai().contains(5)) {
                finisave.add(obj);
            }
        }
        return finisave;
    }

    public static Map<Integer, Begikas> skaityti(String failas) {
        Map<Integer, Begikas> begikai = new HashMap<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer begikuSk = Integer.parseInt(eilute);
            for (int i = 0; i < begikuSk; i++) {
                eilute = skaitytuvas.readLine();
                String[] eilDuomenys = eilute.split(" ");
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                LocalTime pradzia = LocalTime.parse(eilDuomenys[2], formatter);
                LocalTime pabaiga = LocalTime.parse(eilDuomenys[3], formatter);
                begikai.put(i+1, new Begikas(i+1, vardas, pavarde, pradzia, pabaiga));
            }

            eilute = skaitytuvas.readLine();
            Integer kiekPunktu = Integer.parseInt(eilute);
            for (int i = 0; i < kiekPunktu; i++) {
                eilute = skaitytuvas.readLine();
                String[] eilDuomenys = eilute.split(" ");
                Integer punktoNr = Integer.parseInt(eilDuomenys[0]);
                begikuSk = Integer.parseInt(eilDuomenys[1]);
                for (int j = 2; j < begikuSk + 2; j++) {
                    Integer begikoNr = Integer.parseInt(eilDuomenys[j]);
                    if (begikai.containsKey(begikoNr)) {
                       Begikas begikas = begikai.get(begikoNr);
                       begikas.getPunktai().add(punktoNr);
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return begikai;
    }
}
