package java20.Begikai;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Begikas {
    private Integer begikoNr;
    private String vardas;
    private String pavarde;
    private LocalTime pradziosLaikas;
    private LocalTime pabaigosLaikas;
    private LocalTime skirtumas;
    private List<Integer> punktai;

    public Begikas(Integer begikoNr, String vardas, String pavarde, LocalTime pradziosLaikas, LocalTime pabaigosLaikas) {
        this.begikoNr = begikoNr;
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.pradziosLaikas = pradziosLaikas;
        this.pabaigosLaikas = pabaigosLaikas;
        this.skirtumas = pabaigosLaikas.minusNanos(pradziosLaikas.toNanoOfDay());
        this.punktai = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Begikas{" +
                "begikoNr=" + begikoNr +
                ", vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", pradziosLaikas=" + pradziosLaikas +
                ", pabaigosLaikas=" + pabaigosLaikas +
                ", skirtumas=" + skirtumas +
                ", Punktai =" + punktai +
                '}' + "\n";
    }

    public Integer getBegikoNr() {
        return begikoNr;
    }

    public void setBegikoNr(Integer begikoNr) {
        this.begikoNr = begikoNr;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public LocalTime getPradziosLaikas() {
        return pradziosLaikas;
    }

    public void setPradziosLaikas(LocalTime pradziosLaikas) {
        this.pradziosLaikas = pradziosLaikas;
    }

    public LocalTime getPabaigosLaikas() {
        return pabaigosLaikas;
    }

    public void setPabaigosLaikas(LocalTime pabaigosLaikas) {
        this.pabaigosLaikas = pabaigosLaikas;
    }

    public LocalTime getSkirtumas() {
        return skirtumas;
    }

    public void setSkirtumas(LocalTime skirtumas) {
        this.skirtumas = skirtumas;
    }

    public List<Integer> getPunktai() {
        return punktai;
    }

    public void setPunktai(List<Integer> punktai) {
        this.punktai = punktai;
    }
}
