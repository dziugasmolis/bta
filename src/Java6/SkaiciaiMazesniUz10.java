package Java6;

import java.util.Arrays;

public class SkaiciaiMazesniUz10 {
    public static void main(String[] args) {
        Integer[] masyvas = {3, 4, 6, 8, 9, 10, 11, 12, 99, 88, 5};
        Integer[] atrinkti = mazesniUz10(masyvas);
        for (int i = 0; i < atrinkti.length; i++) {
            System.out.println(atrinkti[i]);
        }
    }

    public static Integer[] mazesniUz10(Integer[] masyvas) {
        Integer[] atrinkti = new Integer[0];
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i] < 10) {
                atrinkti = pridetiElementa(atrinkti, masyvas[i]);
            }
        }
        return atrinkti;
    }

    public static Integer[] pridetiElementa(Integer[] masyvas, Integer skaicius) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = skaicius;
        return masyvas;
    }
}
