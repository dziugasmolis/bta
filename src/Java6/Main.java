package Java6;


import java.text.ParseException;

public class Main {
    public static void main(String[] args) {
        Integer[] sveikuSkaiciuMasyvas = {1, 2, 3, 4, 5};
//        Integer suma = 0;
//        for (int i = 0; i < sveikuSkaiciuMasyvas.length; i++) {
//            if (i <= 2) {
//                continue;
//            }
//            suma = suma + sveikuSkaiciuMasyvas[i];
//
//        }
//        System.out.println("Suma = " + suma);
        try {
            System.out.println(sveikuSkaiciuMasyvas[6]);
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Nera tokio masyvo elemento");
        } finally {
            System.out.println("atejo i finally");
        }
        System.out.println("AAAAAAAAA");
        Integer skaicius = 0;
        try {
            skaicius = Integer.parseInt("a");
        } catch (NumberFormatException ex) {
            System.out.println("Nepavyko paversti");
        }
        System.out.println("BBBBBBB");
        Integer[] pakeistasMasyvas = grazintiMasyvoElementa(sveikuSkaiciuMasyvas, 1, 5);
        for(int i = 0; i < pakeistasMasyvas.length; i++) {
            System.out.println(pakeistasMasyvas[i]);
        }
    }

    public static Integer[] grazintiMasyvoElementa(Integer[] masyvas,
                                                 Integer indexas, Integer reiksme) {

        try {
            masyvas[indexas] = reiksme;
            return masyvas;
        } catch (ArrayIndexOutOfBoundsException ex) {
            return masyvas;
        }
    }
}
