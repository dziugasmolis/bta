package Java6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import static java.lang.System.exit;

public class Skaitymas {
    public static void main(String[] args) {
        String netikra = new File("").getAbsolutePath()
                + "/src/Java4/.txt"; // Duomenu failo direktorija
        String duomenuFailoDirektorija = new File("").getAbsolutePath()
                + "/src/Java6/Duomenys.txt"; // Duomenu failo direktorija
        try {
            skaityti(duomenuFailoDirektorija);

        } catch (FileNotFoundException ex) {
            System.out.println("Nerado failo");
            try {
                skaityti(duomenuFailoDirektorija);
            } catch (Exception ex1) {
                System.out.println("Atejo i exception bloka esanti viduje");
            }
        } catch (Exception ex) {
            System.out.println("Atejo i exception bloka");
        } finally {
            System.out.println("Atejo i finally");
        }
    }
    public static void skaityti(String failas)
            throws Exception,FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(failas));
        String eilute = br.readLine();
        while (eilute != null) {
            System.out.println(eilute);
            eilute = br.readLine();
            throw new Exception("Nera failo");
        }

    }

    public static void skaityti1(String failas) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(failas));
            String eilute = br.readLine();
            while (eilute != null) {
                System.out.println(eilute);
                eilute = br.readLine();
                throw new Exception("Nera failo");
            }
        } catch (Exception ex) {

        }

    }
}
