package java11.Bilietai;

import java10.Keliautojai.Keliautojai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String duomenuFailoKelias = new File("").getAbsolutePath() +
                "/src/java11/Bilietai/Duomenys.txt";
        Integer[][] duomenys = skaitymas(duomenuFailoKelias);
        System.out.println(visaSumaPinigu(duomenys));
        System.out.println(vidurkis(duomenys));
        System.out.println(daugiausiaiUzdirbusiEilute(duomenys));
        System.out.println(daugiausiaiZmoniuEileje(duomenys));
    }

    public static Integer[][] skaitymas(String file) {
        Integer[][] masyvas = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine(); // Nuskaito pirma eilute
            String[] eilutesReiksmes = eilute.split(" "); // Pirma eilute isskiriam pagal tarpa
            Integer kiekisEiluciu = Integer.parseInt(eilutesReiksmes[0]); // paverciam pirmos eilute pirma nari i Integer
            Integer kiekisStulpeliu = Integer.parseInt(eilutesReiksmes[1]); // paverciam pirmos eilute antra nari i Integer
            masyvas = new Integer[kiekisEiluciu][kiekisStulpeliu]; // Sukuriam masyva pagal tikslu dydi
            for (int i = 0; i < kiekisEiluciu; i++) {
                eilute = skaitytuvas.readLine(); // Kiekvieno ciklo metu nuskaitys eilute
                eilutesReiksmes = eilute.split(" "); // Isskaidom eilutes reiksmes pagal tarpa
                for (int j = 0; j < kiekisStulpeliu; j++) {
                    masyvas[i][j] = Integer.parseInt(eilutesReiksmes[j]); // Priskiriam eilutes viena reiksme i masyva
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return masyvas; // grazinam sukurta objekta
    }

    public static Double visaSumaPinigu(Integer[][] uzimtosVietos) {
        Double suma = 0d;
        for (int i = 0; i < uzimtosVietos.length; i++) {
            Integer kiekisAtvykusiu = 0;
            for (int j = 0; j < uzimtosVietos[i].length; j++) {
                if (uzimtosVietos[i][j].equals(1)) { // Tikrinam atvykusius
                    kiekisAtvykusiu++;
                }
            }
            suma += grazintiPelnaPaleiEilutes(i, kiekisAtvykusiu);
        }
        return suma;
    }

    public static Double vidurkis(Integer[][] masyvas) {
        Double vid = 0d;
        Integer atvykusiuZmoniuSk = 0;
        for (int i = 0; i < masyvas.length; i++) {
            atvykusiuZmoniuSk += grazintiAtvykusiuZmoniuSkaiciu(masyvas, i);
        }
        vid = visaSumaPinigu(masyvas) / atvykusiuZmoniuSk;
        return vid;
    }

    public static Integer grazintiAtvykusiuZmoniuSkaiciu(Integer[][] masyvas, Integer eilesNr) {
        Integer kiekAtvyko = 0;
        for (int i = 0; i < masyvas[eilesNr].length; i++) {
            if (masyvas[eilesNr][i].equals(1)) {
                kiekAtvyko++;
            }
        }
        return kiekAtvyko;
    }

    public static Integer daugiausiaiUzdirbusiEilute(Integer[][] masyvas) {
        Integer eilutesNr = 0;
        Double maxUzdirbusiEile = 0d;
        for (int i = 0; i < masyvas.length; i++) {
            Integer atvykeZmones = grazintiAtvykusiuZmoniuSkaiciu(masyvas, i);
            Double eilutesPelnas = grazintiPelnaPaleiEilutes(i, atvykeZmones);
            if (eilutesPelnas > maxUzdirbusiEile) {
                maxUzdirbusiEile = eilutesPelnas;
                eilutesNr = i;
            }
        }
        return eilutesNr;
    }

    public static Integer daugiausiaiZmoniuEileje(Integer[][] masyvas) {
        Integer eilutesNr = 0;
        Integer maxEileje = 0;
        for (int i = 0; i < masyvas.length; i++) {
            Integer kiekAtvyko = grazintiAtvykusiuZmoniuSkaiciu(masyvas, i);
            if (kiekAtvyko > maxEileje) {
                maxEileje = kiekAtvyko;
                eilutesNr = i;
            }
        }
        return eilutesNr;
    }


    public static Double grazintiPelnaPaleiEilutes(Integer eilutesNr, Integer kiekAtvyko) {
        Double suma = 0d;
        if (eilutesNr == 0 || eilutesNr == 1) { // Ziurime kuri eilute ir pagal ja skaiciuojam kaina
            suma += 100 * kiekAtvyko;
        } else if (eilutesNr == 2 || eilutesNr == 3) {
            suma += 70 * kiekAtvyko;
        } else {
            suma += 40 * kiekAtvyko;
        }
        return suma;
    }
}
