package java11.VidineKlase;

public class Main {
    public static void main(String[] args) {
        Isorine isorine = new Isorine();
        Isorine.Vidine obj = new Isorine.Vidine();
        System.out.println(obj.getTest());
        obj.setTest("Nauja reiksme");
        System.out.println(obj.getTest());

        Isorine.Vidine obj2 = new Isorine.Vidine();
        System.out.println(obj2.getTest());
        obj2.setTest("Objektas 2");
        System.out.println(obj2.getTest());
    }
}
