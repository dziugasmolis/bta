package java11.VidineKlase;

public class Isorine {
    public Isorine() {

    }
    static class Vidine {
        private String test = "a";

        public Vidine() {
            System.out.println(test);
        }

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }
    }
}
