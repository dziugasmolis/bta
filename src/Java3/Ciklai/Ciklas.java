package Java3.Ciklai;

public class Ciklas {
    public static void main(String[] args) {

//        int[] test = {1,2,3,4,5};
//
//        for (int tarpinis: test) {
//            System.out.println(tarpinis);
//        }


//        Integer suma = 0;
//        for(int i = 0; e < 100; i++) {
////
////            if(i % 2 == 0) {
//                continue;
//            }
//            System.out.println("i = " + i);
//            suma += i;
//            System.out.println("Suma = " + suma);
//
//        }
//        System.out.println("Isejom is ciklo");
//        int suma = 0;
//
//        suma += 1;
//        suma = suma + 1;
//        for(int i = 100; i > 0; i--) {
//            System.out.println(i);
//        }
//        int i = 0;
//        Integer suma = 0;
//        while (suma < 11) {
//            System.out.println("i = " + i);
//            System.out.println("suma = " + suma);
//            suma += i;
//            i++;
//        }
//        int i = 0;
//        Integer a = 0;
//        Double skaiciai = 0.0;
//        do {
//            System.out.println(i);
//            i++;
//        } while (i < 10);


//        Integer suma = 0;
//        for(int i = 0; i < 15; i++) {
//
//            suma += i;
//        }
//
//        Double vid = suma / 15.0;
//        System.out.println(vid);

        Integer diena = 10;
        switch (diena) {
            case 1:
                System.out.println("Pirmadienis");
                break;
            case 2:
                System.out.println("Antradienis");
                break;
            case 3:
                System.out.println("Treciadienis");
                break;
            case 4:
                System.out.println("Ketvirtadienis");
                break;
            case 5:
                System.out.println("Penktadienis");
                break;
            case 6:
                System.out.println("Sestadienis");
                break;
            case 7:
                System.out.println("Sekmadienis");
                break;
            default:
                System.out.println("Tokios dienos nera");
        }
    }
}
