package Java3.SkaiciuNuskaitymas;

import java.io.*;

public class SkaiciuNuskaitymas {

    public static void main(String[] args) {

        String duomenuFailoDirektorija = new File("").getAbsolutePath()
                + "/src/Java3/SkaiciuNuskaitymas/duomenys.txt"; // Duomenu failo direktorija
        String rezultatuFailoDirektorija = new File("").getAbsolutePath()
                + "/src/Java3/SkaiciuNuskaitymas/rezultatai.txt"; // Rezultato failo direktorija
//        String visosEilutes = "";
        Integer skaicius = 0;
        try (BufferedReader failoSkaitytuvas =
                     new BufferedReader(new FileReader(duomenuFailoDirektorija))) { // atidaromas skaitomas failas
            String eilute = failoSkaitytuvas.readLine(); // nuskaitoma pirma eilute
            try (BufferedWriter rasymas = new BufferedWriter(new FileWriter(rezultatuFailoDirektorija))) {
                while (eilute != null) { // kol eilute netuscia tol skaitys
                    System.out.println(eilute);
                    String[] isskaidytaEilute = eilute.split(" "); // isskiriame eilutes skaicius per tarpeli
                    for (int i = 0; i < isskaidytaEilute.length; i++) { // einame per skaicius
                        skaicius = Integer.parseInt(isskaidytaEilute[i]); // paverciame string skaiciu i integer tipa
                        // sukuriame rezultatu faila
                        if (skaicius % 2 == 0) {
                            rasymas.write("Skaicius " + skaicius + " yra lyginis" + "\n"); // i rezultatu faila irasome eilute
                        } else {
                            rasymas.write("Skaicius " + skaicius + " nera lyginis" + "\n");
                        }

                    }
                    eilute = failoSkaitytuvas.readLine(); // nuskaitome zemiau esancia eilute
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }

    }
}
