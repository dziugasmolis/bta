package java10;

interface PirmasInterface {

    Double plotas();

    default Double perimetras() {
        return null;
    }

    static void InterfaceStatinisMetodas() {
        System.out.println("Interface Statinis");
    };

}
