package java10;

public class Staciakampis implements PirmasInterface, AntrasInterface {
    private Integer a;
    private Integer b;

    public Staciakampis(Integer a, Integer b) {
        this.a = a;
        this.b = b;
    }

    public void Test(String a) {
        System.out.println("test");
    }

    public static void statinisMetodas() {
        System.out.println("Statinis metodas");
    }

    @Override
    public Double plotas() {
        return a * 1.0 * b;
    }

    @Override
    public Double perimetras() {
        return a + b * 8.0;
    }

    public Double getPerimetas() {
        return perimetras();
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }
}
