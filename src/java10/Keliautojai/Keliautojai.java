package java10.Keliautojai;

import java.util.Arrays;

public class Keliautojai {
    private Double[] svoriai;
    private Double kainaKg;

    public Keliautojai(Double[] svoriai, Double kainaKg) {
        this.svoriai = svoriai;
        this.kainaKg = kainaKg;
    }

    @Override
    public String toString() {
        return "Keliautojai{" +
                "svoriai=" + Arrays.toString(svoriai) +
                ", kainaKg=" + kainaKg +
                '}';
    }

    public Double[] getSvoriai() {
        return svoriai;
    }

    public void setSvoriai(Double[] svoriai) {
        this.svoriai = svoriai;
    }

    public Double getKainaKg() {
        return kainaKg;
    }

    public void setKainaKg(Double kainaKg) {
        this.kainaKg = kainaKg;
    }
}
