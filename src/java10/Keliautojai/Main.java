package java10.Keliautojai;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String duomenuFailoKelias = new File("").getAbsolutePath() +
                "/src/java10/Keliautojai/Duomenys.txt";
        String rezultatai = new File("").getAbsolutePath() +
                "/src/java10/Keliautojai/Rezultatai.txt";

        Keliautojai keliautojai = skaitymas(duomenuFailoKelias); // gaunam nuskaitytus duomenis
        System.out.println(Arrays.toString(keliautojai.getSvoriai()));
        System.out.println(keliautojai.getKainaKg());
        keliautojai.setKainaKg(3d);
        System.out.println(keliautojai.getKainaKg());
        rasymas(rezultatai,keliautojai);
    }

    public static Keliautojai skaitymas(String file) {
        Keliautojai obj = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine(); // Nuskaito pirma eilute
            Integer kiekisKeliautoju = Integer.parseInt(eilute); // paverciam pirma eilute i Integer
            eilute = skaitytuvas.readLine(); // Nuskaito antra eilute
            String[] eilutesReiksmes = eilute.split(" "); // antra eilute isskaidom i atskirus string
            Double[] svoriai = new Double[kiekisKeliautoju]; // susikuriam svoriu masyva pagal keliautoju skaiciu
            for (int i = 0; i < kiekisKeliautoju; i++) { // sukam cikla kad idet i masyva svorius
                svoriai[i] = Double.parseDouble(eilutesReiksmes[i]); // Dedam svorius i masyva
            }
            eilute = skaitytuvas.readLine(); // Nuskaitom trecia eilute
            Double kainaKg = Double.parseDouble(eilute); // paverciam trecia eilute i double
            obj = new Keliautojai(svoriai, kainaKg); // sukuriam objekta kuri grazinsim

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return obj; // grazinam sukurta objekta
    }

    public static Double koksSvoris(Double[] svoriai) { // Nurodom kad grazinsim Double tipa, metodo pavadinima ir parametrus
        Double suma = 0d;
        for(int i = 0; i < svoriai.length;i++) {
            suma += svoriai[i];
        }
        return suma;
    }

    public static Double bagazoKaina(Keliautojai keliautojai) {
        return koksSvoris(keliautojai.getSvoriai()) * keliautojai.getKainaKg();
    }

    public static Double sunkiausiasBagazas(Double[] svoriai) {
        Double max = 0d;
        for(int i = 0; i < svoriai.length; i++) {
            if(svoriai[i] > max) {
                max = svoriai[i];
            }
        }
        return max;
    }

    public static void rasymas(String rezultatai, Keliautojai keliautojai) {
        try (BufferedWriter rasytojas = new BufferedWriter(new FileWriter(rezultatai))) {
            rasytojas.write(koksSvoris(keliautojai.getSvoriai()).toString() + "kg\n");  // Raso i pirma rezultatu eilute
            rasytojas.write(bagazoKaina(keliautojai).toString() + "Lt\n"); // Raso i antra rezultatu eilute
            rasytojas.write(sunkiausiasBagazas(keliautojai.getSvoriai()).toString()); // Raso i trecia rezultatu eilute
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}