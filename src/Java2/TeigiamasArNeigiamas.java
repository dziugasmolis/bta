package Java2;

import java.util.Scanner;

public class TeigiamasArNeigiamas {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite skaiciu: ");
        int skaicius = skaitytuvas.nextInt();
        skaitytuvas.close();
        if(skaicius > 0) {
            System.out.println("Skaicius yra teigiamas");
        } else if (skaicius < 0) {
            System.out.println("Skaicius yra neigimas");
        } else {
            System.out.println("Skaicius yra lygus nuliui");
        }
    }
}
