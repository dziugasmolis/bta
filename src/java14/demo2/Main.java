package java14.demo2;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {

        System.out.println("HashSet:");
        testSet(new HashSet<>());

        System.out.println("--------------------");

        System.out.println("TreeSet:");
        testSet(new TreeSet<>());
    }


    private static void testSet(Set<String> set) {

        set.add("aaa");
        set.add("bbb");
        set.add("ccc");
        set.add("ddd");
        set.add("eee");

        System.out.print("Aibės dydis " + set.size() + ": " );
        for (String i : set) {
            System.out.print(" " + i);
        }
        System.out.println();

//        System.out.println("Ar yra 20? = " + set.contains(20));

//        System.out.println("Išmetam is sąrašo 20");
//        set.remove(20);

        System.out.print("Aibės dydis " + set.size() + ": " );
        for (String i : set) {
            System.out.print(" " + i);
        }
        System.out.println();

    }
}
