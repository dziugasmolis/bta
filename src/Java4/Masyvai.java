package Java4;

import java.util.Arrays;

public class Masyvai {
    public static void main(String[] args) {
        int[] masyvas = new int[10];
        int[] priskirtaReiksme = {1,2,3,45};
        System.out.println(priskirtaReiksme[priskirtaReiksme.length - 1]);

        int[][] b = {{2, 4}, {3, 9}, {5, 25}};
        int y = b[1][0];
        System.out.println(y);

        int[] naujasMasyvas = Arrays.copyOf(priskirtaReiksme,
                priskirtaReiksme.length - 1);

        for (int i = 0; i < b.length; i++) {

            for (int j = 0; j < b[i].length; j++) {
                System.out.print(b[i][j] + ",");
            }
            System.out.println("\n");
        }
    }
}
