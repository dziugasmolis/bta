package Java4;

import java.util.Arrays;

public class Vidurkis {
    public static void main(String[] args) {
        int[] masyvas = {2, 5, 8, 6, 9, 7};
        int suma = 0;
        for (int i = 0; i < masyvas.length; i++) {
            suma = suma + masyvas[i];
        }
        Double vid = suma / masyvas.length * 1.0;
        System.out.println("Suma = " + suma);
        System.out.println("Vidurkis = " + vid);

        int[] didesniUzVidurki = new int[0];
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i] >= vid) {
                didesniUzVidurki = Arrays.copyOf(didesniUzVidurki,
                        didesniUzVidurki.length + 1);
                didesniUzVidurki[didesniUzVidurki.length - 1]
                        = masyvas[i];
            }
        }

        for (int i = 0; i < didesniUzVidurki.length; i++) {
            System.out.println(didesniUzVidurki[i]);
        }
    }
}
