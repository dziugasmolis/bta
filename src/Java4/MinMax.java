package Java4;

public class MinMax {
    public static void main(String[] args) {
        int[] masyvas = {2, 5, 6, 9, 2, -2, 5, -3, 4};
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i] < min) {
                min = masyvas[i];
            }
        }

        System.out.println("Maziausias = " + min);

        int max = 0;

        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i] > masyvas[max]) {
                max = i;
            }
        }

        System.out.println("Didziausias = " + masyvas[max]);
    }
}
