package Java4;

public class MasyvoApsukimas {
    public static void main(String[] args) {
        int[] masyvas = {1, 2, 3, 4, 5};
        int[] apsuktasMasyvas = new int[masyvas.length];
        int tarpinisIndeksas = 0;
        for (int i = masyvas.length - 1; i >= 0; i--) {
            System.out.println(masyvas[i]);
            apsuktasMasyvas[i] = masyvas[tarpinisIndeksas++];
        }
        System.out.println("\n");
        for (int i = 0; i < apsuktasMasyvas.length; i++) {
            System.out.println(apsuktasMasyvas[i]);
        }

    }
}
