package Java5;

public class Main {
    public static void main(String[] args) {
//        daugParametru(1);
//        daugParametru(1, 2);
//        daugParametru(1, 2, 3);

//        int a = 100;
//        System.out.println("Pradine reiksme = " + a);
//        a = pakeistiReiksme(a);
//        System.out.println("Pakeista reiksme = " + a);
        String zodis = "aaa";
        Integer[] skaiciai = {1,3,4};
        Boolean arTuriSkaiciuDu = arTuriSkaiciuDu(skaiciai);
//        spausdinti(skaiciai);
//        System.out.println(arTuriSkaiciuDu);
        System.out.println("Pradinis masyvas ");
//        spausdinti(skaiciai);
        changeValue(skaiciai);
        System.out.println("Paredaguotas masyvas");
//        spausdinti(skaiciai);
//        String a = "Labas";
////
//        changeString(a);
//        String b = a;
//        System.out.println("A reiksme " + a);
//        System.out.println("B reiksme " + b);

        Integer skaicius = 1;
        pakeistiInteger(skaicius);
        System.out.println("Skaicius = " + skaicius);

        daugParametru(1,2);
    }

    private static void changeString(String a) {
        a = a + " Vakaras";
        System.out.println(a);
    }

    private static void pakeistiInteger(Integer a) {
        a += 99;
        System.out.println(a);
    }


    private static void changeValue(Integer[] array){
        array = new Integer[]{1,2,3,4,5,6};
    }

    public static void spausdinti(Integer[] masyvas) {
        for(int i = 0; i < masyvas.length; i++) {
            System.out.println(masyvas[i]);
        }
    }


    public static int pakeistiReiksme(int a) {
        return 1;
//        System.out.println("Reiksme metode = " + a);
    }

    public static void daugParametru(Integer... a) {
        if(a.length == 1) {
          System.out.println(a[0]);
        }
        if(a.length == 2) {
            System.out.println(a[1]);
        }
        if(a.length == 3) {
            System.out.println(a[2]);
        }

    }

//    public static void spausdinti(Integer[] masyvas) {
//        for(int i = 0; i < masyvas.length; i++) {
//            if(masyvas[i] == 2) {
//                return;
//            }
//            System.out.println(masyvas[i]);
//        }
//    }

    public static Boolean arTuriSkaiciuDu(Integer[] masyvas) {
        for(int i = 0; i < masyvas.length; i++) {
            if(masyvas[i] == 2) {
                return true;
            }
//            System.out.println(masyvas[i]);
        }
        return false;
    }
}
