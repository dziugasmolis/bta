package Java7;

import java.util.Arrays;

public class MokinioSkaiciavimai {
    public static void main(String[] args) {
        Mokinys mokinys1 = new Mokinys("Jonas", "Jonaitis",
                5, new Integer[]{4,2,6,8});
        Mokinys mokinys2 = new Mokinys("Petras", "Jonaitis",
                5, new Integer[]{4,9,7,8});
        Mokinys mokinys3 = new Mokinys("Andrius", "Jonaitis",
                5, new Integer[]{10,10,10,10});
        Mokinys mokinys4 = new Mokinys("Vasia", "Jonaitis",
                5, new Integer[]{2,2,2,2,2});
        Mokinys[] mokiniai = new Mokinys[]{mokinys1, mokinys2,
                mokinys3,mokinys4};
        Double vidurkis = visuMokiniuPazymiuSuma(mokiniai) * 1.0 / visuMokiniuPazymiuKiekis(mokiniai)
                ;
        System.out.println("Vidurkis = " + vidurkis);
        System.out.println("Geriausias mokinys: " + rastiGeriausiaMokini(mokiniai));
    }

    public static Mokinys rastiGeriausiaMokini(Mokinys[] mokiniai) {
        Double max = 0d;
        Integer maxVieta = 0;
        for (int i = 0; i < mokiniai.length; i++) {
            if(mokiniai[i].getPazymiuVidurkis() > max) {
                max = mokiniai[i].getPazymiuVidurkis();
                maxVieta = i;
            }
        }
        return mokiniai[maxVieta];
    }

    public static Integer visuMokiniuPazymiuSuma(Mokinys[] mokiniai) {
        Integer suma = 0;
        for (int i = 0; i < mokiniai.length; i++) {
            suma += mokiniai[i].getPazymiuSuma();
        }
        return suma;
    }

    public static Integer visuMokiniuPazymiuKiekis(Mokinys[] mokiniai) {
        Integer pazymiuKiekis = 0;
        for (int i = 0; i < mokiniai.length; i++) {
            pazymiuKiekis += mokiniai[i].getPazymiuMasyvas().length;
        }
        return pazymiuKiekis;
    }
}
