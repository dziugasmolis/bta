package Java7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class VejoJegaineSkaiciavimai {

    public static void main(String[] args) {
        String duomenuFailoPath = new File("").getAbsolutePath() + "/src/Java7/duomenys.txt";
        Jegaine[] nuskaitytiDuomenys = skaityti(duomenuFailoPath);
//        System.out.println(Arrays.toString(nuskaitytiDuomenys));
        System.out.println(kiekKartuPuteKryptiesVejas(nuskaitytiDuomenys, 1) + " " +
                kiekKartuPuteKryptiesVejas(nuskaitytiDuomenys, 2) + " " +
                kiekKartuPuteKryptiesVejas(nuskaitytiDuomenys, 3) + " " +
                kiekKartuPuteKryptiesVejas(nuskaitytiDuomenys, 4) + " ");

        if(arAtitinkaJegainesGreiciai(nuskaitytiDuomenys)) {
            System.out.println("TAIP");
        } else {
            System.out.println("NE");
        }
    }

    public static Jegaine[] skaityti(String failas) {
        Jegaine[] masyvas = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer eiluciuKiekis = Integer.parseInt(eilute);
            masyvas = new Jegaine[eiluciuKiekis];
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < eiluciuKiekis; i++) {
                String[] reiksmes = eilute.split(" ");
                Jegaine jeg = new Jegaine(Integer.parseInt(reiksmes[0]), Double.parseDouble(reiksmes[1]));
                masyvas[i] = jeg;
                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return masyvas;
    }

    public static Integer kiekKartuPuteKryptiesVejas(Jegaine[] masyvas, Integer norimaKryptis) {
        Integer kiekKartu = 0;
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i].getKryptis() == norimaKryptis) {
                kiekKartu++;
            }
        }
        return kiekKartu;
    }

    public static Boolean arAtitinkaJegainesGreiciai(Jegaine[] masyvas) {
        for (int i = 0; i < masyvas.length; i++) {
            if(masyvas[i].getVidutinisGreitis() < 5d) {
                return false;
            }
        }
        return true;
    }
}
