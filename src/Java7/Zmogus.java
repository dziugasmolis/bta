package Java7;

public class Zmogus {
    private String vardas;
    private String pavarde;
    private Integer amzius;
    private Boolean galiDirbti;
    private Boolean valid;

    public Zmogus() {
        vardas = "";
        pavarde = "";
        amzius = 0;
    }

    public Zmogus(String vardas, String pavarde, Integer amzius) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
    }

    public String toString() {
        return "Vardas = " + getVardas() + " Pavarde = "
                + getPavarde() + " Amzius = " + getAmzius();
    }

    public void setVardasIrPavarde(String vardas, String pavarde) {
        this.vardas = vardas;
        this.pavarde = pavarde;
    }

    public String getVardas() {
        if(vardas == null) {
            return "";
        }
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public Integer getAmzius() {
        return amzius;
    }

    public void setAmzius(Integer amzius) {
        this.amzius = amzius;
    }

    public Boolean isGaliDirbti() {
        return galiDirbti;
    }

    public void setGaliDirbti(Boolean galiDirbti) {
        this.galiDirbti = galiDirbti;
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
