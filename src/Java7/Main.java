package Java7;

public class Main {
    public static void main(String[] args) {
        Zmogus klase = new Zmogus("Jonas",
                "Jonaitis", 99);
        Zmogus tusciaKlase = new Zmogus();

//        tusciaKlase.setVardasIrPavarde("vardas", "pavarde");
//
        System.out.println(klase.toString());
        System.out.println(tusciaKlase);
        ProtectedMetodas.testas();
    }
}
