package Java7;

public class Jegaine {
    private Integer kryptis;
    private Double vidutinisGreitis;

    public Jegaine(Integer kryptis, Double vidutinisGreitis) {
        this.kryptis = kryptis;
        this.vidutinisGreitis = vidutinisGreitis;
    }

    public String toString() {
        return "Kryptis: " + getKryptis() + " Greitis: " + getVidutinisGreitis();
    }

    public Integer getKryptis() {
        return kryptis;
    }

    public void setKryptis(Integer kryptis) {
        this.kryptis = kryptis;
    }

    public Double getVidutinisGreitis() {
        return vidutinisGreitis;
    }

    public void setVidutinisGreitis(Double vidutinisGreitis) {
        this.vidutinisGreitis = vidutinisGreitis;
    }
}
