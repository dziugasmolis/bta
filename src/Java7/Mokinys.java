package Java7;

import java.util.Arrays;

public class Mokinys {
    private String vardas;
    private String pavarde;
    private Integer klase;
    private Integer[] pazymiuMasyvas;

    public Mokinys(String vardas, String pavarde,
                   Integer klase, Integer[] pazymiuMasyvas) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        this.pazymiuMasyvas = pazymiuMasyvas;
    }

    public String toString() {
        return "Vardas: " + getVardas() + " Pavarde: " + getPavarde() +
                "Klase: " + getKlase()
                + "Pazymiu Masyvas: " + Arrays.toString(getPazymiuMasyvas());
    }

    public Integer getPazymiuSuma() {
        Integer suma = 0;
        for(Integer paz: getPazymiuMasyvas()) {
            suma += paz;
        }
        return suma;
    }

    public Double getPazymiuVidurkis() {
        return getPazymiuSuma() * 1.0 / getPazymiuMasyvas().length;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }

    public Integer[] getPazymiuMasyvas() {
        return pazymiuMasyvas;
    }

    public void setPazymiuMasyvas(Integer[] pazymiuMasyvas) {
        this.pazymiuMasyvas = pazymiuMasyvas;
    }
}
