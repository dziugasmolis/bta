package java9.Pauksciai;

public class PauksciuKonteineris {

    private Paukstis[] pauksciuMasyvas;

    public PauksciuKonteineris(Paukstis[] pauksciuMasyvas) {
        this.pauksciuMasyvas = pauksciuMasyvas;
    }

    public Paukstis rastiVyriausiaPauksti() {
        Paukstis obj = null;
        Integer amziusMax = 0;
        for (int i = 0; i < this.pauksciuMasyvas.length; i++) {
            if(amziusMax < this.pauksciuMasyvas[i].gautiGyvenimoAmziu()) {
                amziusMax = this.pauksciuMasyvas[i].gautiGyvenimoAmziu();
                obj = this.pauksciuMasyvas[i];
            }
        }
        return obj;
    }

    public Integer kiekVarnu() {
        Integer kiek = 0;

        for (int i = 0; i < this.pauksciuMasyvas.length; i++) {
            if(this.pauksciuMasyvas[i] instanceof Varna) {
                kiek++;
            }
        }

        return kiek;
    }

    public Paukstis[] getPauksciuMasyvas() {
        return pauksciuMasyvas;
    }

    public void setPauksciuMasyvas(Paukstis[] pauksciuMasyvas) {
        this.pauksciuMasyvas = pauksciuMasyvas;
    }
}
