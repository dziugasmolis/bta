package java9.Pauksciai;

public class Varna extends Paukstis {

    private String pavadinimas;
    private Integer amzius;
    private Double svoris;

    public Varna(String pavadinimas, Integer amzius,
                    Double svoris) {
        this.pavadinimas = pavadinimas;
        this.amzius = amzius;
        this.svoris = svoris;
    }

    @Override
    public String toString() {
        return "Varna{" +
                "pavadinimas='" + pavadinimas + '\'' +
                ", amzius=" + amzius +
                ", svoris=" + svoris +
                '}';
    }

    public String gautiPavadinima() {
        return pavadinimas;
    }

    @Override
    public Integer gautiGyvenimoAmziu() {
        return amzius;
    }

    @Override
    public Double gautiSvori() {
        return svoris;
    }
}
