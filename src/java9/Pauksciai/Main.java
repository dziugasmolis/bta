package java9.Pauksciai;

public class Main {
    public static void main(String[] args) {
       Zvirblis zvirblis1 = new Zvirblis("Zvirblis",
                2,0.5);
        Zvirblis zvirblis2 = new Zvirblis("Zvirblis",
                3,1.5);
        Varna varna1 = new Varna("Varna2", 1, 1d);
        Varna varna2 = new Varna("Varna", 9, 1d);
        Paukstis[] masyvas = {zvirblis1, varna1, varna2};
        PauksciuKonteineris konteineris = new PauksciuKonteineris(masyvas);

        System.out.println(konteineris.rastiVyriausiaPauksti());
        System.out.println("Varnu kiekis = " + konteineris.kiekVarnu());

    }
}
