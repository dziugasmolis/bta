package java9.Figuros;

public class Main {
    public static void main(String[] args) {
//        Figura obj = new Figura();

        Staciakampis naujasStaciakampis1 = new Staciakampis(2, 3);
        Staciakampis naujasStaciakampis2 = new Staciakampis(9, 3);

//        System.out.println(naujasStaciakampis);
        Figura[] masyvas = new Figura[]{
                naujasStaciakampis1,
                naujasStaciakampis2
        };
        System.out.println(rastiDidziausiaPerimetra(masyvas));

    }

    public static Figura rastiDidziausiaPerimetra(Figura[] masyvas) {
        Integer maxPerimetras = 0;
        Integer didziausioIndeksas = 0;
        for (int i = 0; i < masyvas.length; i++) {
            if (maxPerimetras < masyvas[i].gautiPerimetra()) {
                maxPerimetras = masyvas[i].gautiPerimetra();
                didziausioIndeksas = i;
            }
        }
        return masyvas[didziausioIndeksas];
    }
}
