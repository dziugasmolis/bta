package java9.Figuros;

public abstract class Figura {
    private Integer skaicius = 0;


    public abstract Integer gautiPerimetra();

    public abstract Integer gautiPlota();

    public Integer getSkaicius() {
        return skaicius;
    }

    public void setSkaicius(Integer skaicius) {
        this.skaicius = skaicius;
    }
}
