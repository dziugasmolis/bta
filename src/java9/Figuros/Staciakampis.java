package java9.Figuros;

public class Staciakampis extends Figura {
    private Integer a;
    private Integer b;

    public Staciakampis(Integer a, Integer b) {
        this.a = a;
        this.b = b;
    }


    public String toString() {
        return "Perimetras = " + gautiPerimetra()
                + " Plotas = " + gautiPlota();
    }

    public Integer gautiPerimetra() {
        return a * 2 + b * 2;
    }
//
    public Integer gautiPlota() {
        return a * b;
    }

    public Integer getA() {
        return this.a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }
}
