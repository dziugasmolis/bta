package java16.Testai;


import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertEquals;

public class ZmogausTestai {

    @Test
    void testasGautiAmziu() {
        Zmogus zmogus = new Zmogus("Jonas", "Jonaitis", 14);
        int amzius = zmogus.getAmzius();
        assertEquals(14, amzius);
    }

    @Test
    void testasGautiVardaPavarde() {
        Zmogus zmogus = new Zmogus("Jonas", "Jonaitis", 14);
        String vardasPavarde = zmogus.getVardasPavarde();
        assertEquals("Jonas Jonaitis", vardasPavarde);
    }
}
