package java16.Tree;

public class Main {
    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();



        bt.add(8);
        bt.add(11);
        bt.add(2);
        bt.add(6);
        bt.add(28);
        bt.add(25);
        bt.add(20);
        bt.traverseInOrder(bt.root);
        System.out.println(bt.containsNode(3));
    }
}
