package java19.prekybaBuitinemisPrekemis;

public class Preke {
    private Integer id;
    private String pavadinimas;
    private Integer prekiusKiekis;
    private Double prekesKaina;

    public Preke(Integer id, String pavadinimas, Integer prekiusKiekis, Double prekesKaina) {
        this.id = id;
        this.pavadinimas = pavadinimas;
        this.prekiusKiekis = prekiusKiekis;
        this.prekesKaina = prekesKaina;
    }

    @Override
    public String toString() {
        return "Preke{" +
                "id=" + id +
                ", pavadinimas='" + pavadinimas + '\'' +
                ", prekiusKiekis=" + prekiusKiekis +
                ", prekesKaina=" + prekesKaina +
                '}' + "\n";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public Integer getPrekiusKiekis() {
        return prekiusKiekis;
    }

    public void setPrekiusKiekis(Integer prekiusKiekis) {
        this.prekiusKiekis = prekiusKiekis;
    }

    public Double getPrekesKaina() {
        return prekesKaina;
    }

    public void setPrekesKaina(Double prekesKaina) {
        this.prekesKaina = prekesKaina;
    }
}
