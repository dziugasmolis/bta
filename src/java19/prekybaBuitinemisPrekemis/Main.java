package java19.prekybaBuitinemisPrekemis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String prekiusFailas = new File("").getAbsolutePath()
                + "/src/java19/prekybaBuitinemisPrekemis/Sandelys.txt";
        String uzsakymuFailas = new File("").getAbsolutePath()
                + "/src/java19/prekybaBuitinemisPrekemis/Uzsakymai.txt";
        Map<Integer, Preke> prekes = skaitytiPrekes(prekiusFailas);
        Map<Integer, Integer> uzsakymai = skaitytiUzsakymus(uzsakymuFailas);
        System.out.println(prekes);
        paskaiciuotiUzsakymus(prekes, uzsakymai);
        System.out.println(prekes);
    }

    public static void paskaiciuotiUzsakymus(Map<Integer, Preke> prekes,
                                             Map<Integer, Integer> uzsakymai) {
        for (Map.Entry<Integer, Integer> entry : uzsakymai.entrySet()) {
            if (prekes.containsKey(entry.getKey())) {
               Preke preke = prekes.get(entry.getKey());
               preke.setPrekiusKiekis(preke.getPrekiusKiekis() - entry.getValue());
            }
        }

    }

    public static Map<Integer, Preke> skaitytiPrekes(String failas) {
        Map<Integer, Preke> mapas = new HashMap<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer prekiuKiekis = Integer.parseInt(eilute);
            for (int i = 0; i < prekiuKiekis; i++) {
                eilute = skaitytuvas.readLine();
                String[] eilReiksmes = eilute.split(" ");
                if (eilReiksmes.length == 5) {
                    String pavadinimas = eilReiksmes[0] + " " + eilReiksmes[1];
                    Integer id = Integer.parseInt(eilReiksmes[2]);
                    Integer kiekis = Integer.parseInt(eilReiksmes[3]);
                    Double kaina = Double.parseDouble(eilReiksmes[4]);
                    mapas.put(id, new Preke(id, pavadinimas, kiekis, kaina));
                } else {
                    String pavadinimas = eilReiksmes[0] + " "
                            + eilReiksmes[1] + " " + eilReiksmes[2];
                    Integer id = Integer.parseInt(eilReiksmes[3]);
                    Integer kiekis = Integer.parseInt(eilReiksmes[4]);
                    Double kaina = Double.parseDouble(eilReiksmes[5]);
                    mapas.put(id, new Preke(id, pavadinimas, kiekis, kaina));
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return mapas;
    }

    public static Map<Integer, Integer> skaitytiUzsakymus(String failas) {
        Map<Integer, Integer> uzsakymai = new HashMap<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer eilKiekis = Integer.parseInt(eilute);
            for (int i = 0; i < eilKiekis; i++) {
                eilute = skaitytuvas.readLine();
                String[] eilReiksmes = eilute.split(" ");
                uzsakymai.put(Integer.parseInt(eilReiksmes[0]), Integer.parseInt(eilReiksmes[1]));
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return uzsakymai;
    }

}

