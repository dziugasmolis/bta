package java19.padangos;

public class Padanga {
    private Integer plotis;
    private Integer aukstis;
    private String skersmuo;
    private Double kaina;

    public Padanga(Integer plotis, Integer aukstis, String skersmuo) {
        this.plotis = plotis;
        this.aukstis = aukstis;
        this.skersmuo = skersmuo;
    }

    public Padanga(Integer plotis, Integer aukstis, String skersmuo, Double kaina) {
        this.plotis = plotis;
        this.aukstis = aukstis;
        this.skersmuo = skersmuo;
        this.kaina = kaina;
    }

    @Override
    public String toString() {
        return "Padanga{" +
                "plotis=" + plotis +
                ", aukstis=" + aukstis +
                ", skersmuo='" + skersmuo + '\'' +
                ", kaina=" + kaina +
                '}';
    }

    public Integer getPlotis() {
        return plotis;
    }

    public void setPlotis(Integer plotis) {
        this.plotis = plotis;
    }

    public Integer getAukstis() {
        return aukstis;
    }

    public void setAukstis(Integer aukstis) {
        this.aukstis = aukstis;
    }

    public String getSkersmuo() {
        return skersmuo;
    }

    public void setSkersmuo(String skersmuo) {
        this.skersmuo = skersmuo;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }
}
