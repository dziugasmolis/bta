package java19.padangos;

import java18.Buitine_Technika.Preke;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String skaitymoFailas = new File("").getAbsolutePath() + "/src/java19/padangos/Duomenys.txt";
        List<Padanga> padangos = new ArrayList<>();
        Padanga ieskoma = skaityti(skaitymoFailas, padangos);
        System.out.println(ieskoma);
        System.out.println(padangos);

    }

    public static Padanga skaityti(String failas, List<Padanga> padangos) {
        Padanga ieskomaPadanga = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            String[] eilDuomenys = eilute.split(" ");
            Integer aukstis = Integer.parseInt(eilDuomenys[0]);
            Integer plotis = Integer.parseInt(eilDuomenys[1]);
            ieskomaPadanga = new Padanga(aukstis, plotis, eilDuomenys[2]);

            eilute = skaitytuvas.readLine();
            Integer padanguSkaicius = Integer.parseInt(eilute);

            for(int i = 0; i < padanguSkaicius; i++){
                eilute = skaitytuvas.readLine();
                eilDuomenys = eilute.split(" ");
                Padanga nuskaityta = null;
                if(eilDuomenys.length == 4) {
                    aukstis = Integer.parseInt(eilDuomenys[0]);
                    plotis = Integer.parseInt(eilDuomenys[1]);
                    String tipas = eilDuomenys[2];
                    Double kaina = Double.parseDouble(eilDuomenys[3]);
                    nuskaityta = new Padanga(aukstis, plotis, tipas, kaina);
                } else if (eilDuomenys.length == 5){
                    aukstis = Integer.parseInt(eilDuomenys[0]);
                    plotis = Integer.parseInt(eilDuomenys[1]);
                    String tipas = eilDuomenys[2] + eilDuomenys[3];
                    Double kaina = Double.parseDouble(eilDuomenys[4]);
                    nuskaityta = new Padanga(aukstis, plotis, tipas, kaina);
                }
                padangos.add(nuskaityta);

            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return ieskomaPadanga;
    }


}
