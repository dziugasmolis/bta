package java8.Avalyne;

public class Avalyne {

    private String tipas;
    private Double kaina;

    public Avalyne(String tipas, Double kaina) {
        this.tipas = tipas;
        this.kaina = kaina;
    }

    public String toString() {
        return "Tipas: " + getTipas() + " Kaina: " + getKaina();
    }

    public String getTipas() {
        return tipas;
    }

    public void setTipas(String tipas) {
        this.tipas = tipas;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }
}
