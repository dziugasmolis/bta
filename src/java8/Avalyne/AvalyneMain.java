package java8.Avalyne;

import java.io.*;

public class AvalyneMain {

    public static void main(String[] args) {
        String duomenuFailoKelias = new File("").getAbsolutePath() +
                "/src/java8/Avalyne/Duomenys.txt";
        String rezultatai = new File("").getAbsolutePath() +
                "/src/java8/Avalyne/Rezultatai.txt";
        Avalyne[] duomenys = skaitymas(duomenuFailoKelias);
        Double vyrSuma = sumaPagalTipa(duomenys, "v");
        Double motSuma = sumaPagalTipa(duomenys, "m");

        Double vyrVid = vidurkisPagalTipa(duomenys, "v", vyrSuma);
        Double motVid = vidurkisPagalTipa(duomenys, "m", motSuma);

        Double bendraSuma = parduotaUz(duomenys);

        rasymas(rezultatai, motSuma, motVid, vyrSuma, vyrVid, bendraSuma);
    }

    public static Double sumaPagalTipa(Avalyne[] masyvas, String tipas) {
        Double suma = 0d;
        for (int i = 0; i < masyvas.length; i++) {
            if(masyvas[i].getTipas().equals(tipas)) {
                suma += masyvas[i].getKaina();
            }
        }
        return suma;
    }

    public static Double vidurkisPagalTipa(Avalyne[] masyvas, String tipas, Double suma) {
        Integer kiekis = 0;
        for(int i = 0; i < masyvas.length; i++) {
            if(masyvas[i].getTipas().equals(tipas)) {
                kiekis++;
            }
        }
        return suma / kiekis;
    }

    public static Avalyne[] skaitymas(String file) {
        Avalyne[] masyvas = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine();
            Integer parduotosBatuPoros = Integer.parseInt(eilute);
            masyvas = new Avalyne[parduotosBatuPoros];
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < parduotosBatuPoros; i++) {
                String[] duomenys = eilute.split(" ");
                Avalyne parduotiBatai = new Avalyne
                        (duomenys[0], Double.parseDouble(duomenys[1]));
                masyvas[i] = parduotiBatai;
                eilute = skaitytuvas.readLine();
            }
//            vyriskiMoteriski(masyvas, "m");


        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return masyvas;
    }

//    public static void vyriskiMoteriski(Avalyne[] masyvas, String tipas) {
//
//        Double sumaVyrisku = 0.0;
//        Integer kiekisVyrisku = 0;
//        Double vidurkisVyrisku;
//        Double sumaMoterisku = 0.0;
//        Integer kiekisMoterisku = 0;
//        Double vidurkisMoterisku;
//        for (int i = 0; i < masyvas.length; i++) {
//            if (masyvas[i].getTipas().equals("m")) {
//                sumaMoterisku += masyvas[i].getKaina();
//                kiekisMoterisku++;
//                System.out.println(masyvas[i].getTipas());
//            } else {
//                sumaVyrisku += masyvas[i].getKaina();
//                kiekisVyrisku++;
//            }
//
//        }
////        System.out.println(Math.round(suma * 100.0) / 100.0);
//        vidurkisMoterisku = sumaMoterisku / kiekisMoterisku;
//        vidurkisVyrisku = sumaVyrisku / kiekisVyrisku;
//        Double bendraSuma = parduotaUz(masyvas);
////        System.out.println(Math.round(vidurkis * 100.0) / 100.0);
//        rasymas(rezultatai, sumaMoterisku, vidurkisMoterisku, sumaVyrisku, vidurkisVyrisku, bendraSuma);
//    }

    public static Double parduotaUz(Avalyne[] masyvas) {
        Double suma = 0.0;
        for (int i = 0; i < masyvas.length; i++) {
            suma += masyvas[i].getKaina();
        }
        return suma;
    }

    public static void rasymas(String rezultatai, Double sumaM, Double vidurkisM, Double sumaV, Double vidurkisV, Double bendraSuma) {
        try (BufferedWriter rasytojas = new BufferedWriter(new FileWriter(rezultatai))) {
            rasytojas.write("Moteriska avalyne:");
            rasytojas.newLine();
            if (sumaM > 0) {
                rasytojas.write("Parduota uz: " + Math.round(sumaM * 100.0) / 100.0 + "eur, vidutine kaina: " + Math.round(vidurkisM * 100.0) / 100.0 + "eur");
            } else {
                rasytojas.write("Prekiauta nebuvo");
            }
            rasytojas.newLine();
            rasytojas.write("Vyriska avalyne:");
            rasytojas.newLine();
            if (sumaV > 0) {
                rasytojas.write("Parduota uz: " + Math.round(sumaV * 100.0) / 100.0 + "eur, vidutine kaina: " + Math.round(vidurkisV * 100.0) / 100.0 + "eur,");
            } else {
                rasytojas.write("Prekiauta nebuvo");
            }
            rasytojas.newLine();
            rasytojas.write("Viso parduota uz: " + Math.round(bendraSuma * 100.0) / 100.0 + "eur,");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
