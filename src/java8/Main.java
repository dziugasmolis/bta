package java8;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Studentas obj = new Studentas(5);
        System.out.println(obj.gautiVardaIrPavarde());
        Zmogus zmogus = new Zmogus("Petras", "Petraitis", 18);
        System.out.println(zmogus);

        Zmogus studentas = new Studentas(8);
//        Studentas a = new Zmogus("Jonas", "jonaitis", 18);
        System.out.println(studentas);

        Test test = new Test("Andrius", "Andrelis", 18, 5, "reiksme");
        System.out.println(test);
    }
}
