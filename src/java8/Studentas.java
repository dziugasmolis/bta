package java8;

import Java7.Jegaine;

public class Studentas extends Zmogus {
    private Integer klase;

    public Studentas(String vardas, String pavarde, Integer amzius, Integer klase) {
        super(vardas, pavarde, amzius);
        this.klase = klase;
    }

    public Studentas(Integer klase) {
        this("a", "b", 12, klase);
    }

    @Override
    public String toString() {
        return "Studentas{" + getVardas() + " " + getPavarde() + " " +getAmzius() +
                " klase=" + klase +
                '}';
    }

    public String gautiVardaIrPavarde() {
        return getVardas() + " " + getPavarde() + " " + getAmzius();
    }

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }
}
