package java8;

public class Test extends Studentas {
    private String reiksme;

    public Test(String vardas, String pavarde, Integer amzius, Integer klase, String reiksme) {
        super(vardas, pavarde, amzius, klase);
        this.reiksme = reiksme;
    }

    public Test(Integer klase, String reiksme) {
        super(klase);
        this.reiksme = reiksme;
    }

    @Override
    public String toString() {
        return "Test{" + getVardas() + " " + getPavarde() + " " + getAmzius() +  " " + getKlase() +
                " reiksme='" + reiksme + '\'' +
                '}';
    }

    public String getReiksme() {
        return reiksme;
    }

    public void setReiksme(String reiksme) {
        this.reiksme = reiksme;
    }
}
