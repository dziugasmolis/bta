package java8.Paveldejimas;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal();

        Dog suo = new Dog();
        Cat kate = new Cat();

        Animal[] masyvas = new Animal[] {suo, kate};
//        Dog[] sunys = new Dog[]{suo, kate};
        for(int i = 0; i < masyvas.length; i++) {
            System.out.println(masyvas[i].toString());
        }
    }
}
