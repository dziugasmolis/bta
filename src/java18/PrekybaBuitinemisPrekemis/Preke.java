package java18.PrekybaBuitinemisPrekemis;

public class Preke {

    private Integer prekesId;
    private String pavadinimas;
    private Double vntKaina;
    private Integer kiekParduota;

    public Preke(Integer prekesId, String pavadinimas, Double vntKaina, Integer kiekParduota) {
        this.prekesId = prekesId;
        this.pavadinimas = pavadinimas;
        this.vntKaina = vntKaina;
        this.kiekParduota = kiekParduota;
    }

    @Override
    public String toString() {
        return "Preke{" +
                "prekesId=" + prekesId +
                ", pavadinimas='" + pavadinimas + '\'' +
                ", vntKaina=" + vntKaina +
                ", kiekParduota=" + kiekParduota +
                '}' + "\n" + "Kiek surinkta: " + kiekSurinkta() + "Lt \n";
    }

    public Double kiekSurinkta() {
        return this.vntKaina * this.kiekParduota;
    }

    public Integer getPrekesId() {
        return prekesId;
    }

    public void setPrekesId(Integer prekesId) {
        this.prekesId = prekesId;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public Double getVntKaina() {
        return vntKaina;
    }

    public void setVntKaina(Double vntKaina) {
        this.vntKaina = vntKaina;
    }

    public Integer getKiekParduota() {
        return kiekParduota;
    }

    public void setKiekParduota(Integer kiekParduota) {
        this.kiekParduota = kiekParduota;
    }
}
