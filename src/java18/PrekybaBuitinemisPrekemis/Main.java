package java18.PrekybaBuitinemisPrekemis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String duomenuFailas = new File("").getAbsolutePath()
                + "/src/java18/PrekybaBuitinemisPrekemis/Duomenys.txt";

        Map<Integer, Preke> prekes = skaityti(duomenuFailas);
        System.out.println(prekes);

        Preke populiariausia  = rastiPopuliariausia(prekes);
        System.out.println(populiariausia);

        List<Integer> skaiciai = new ArrayList<>();
        String zodis = idetiSkaicius(skaiciai);
        System.out.println(zodis);
        System.out.println(skaiciai);
    }

    public static String idetiSkaicius(List<Integer> skaiciai) {
        skaiciai.add(1);
        skaiciai.add(2);
        return "Abcd";
    }

    public static Map<Integer, Preke> skaityti(String failas) {
        Map<Integer, Preke> duomenys = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(failas))) {
            String eilute = br.readLine();
            Integer prekiuKiekis = Integer.parseInt(eilute);
            String[] eilReiksmes = null;
            for (int i = 0; i < prekiuKiekis; i++) {
                eilute = br.readLine();
                eilReiksmes = eilute.split(" ");
                Integer prekesId = Integer.parseInt(eilReiksmes[0]);
                String prekesPavadinimas = eilReiksmes[1] + eilReiksmes[2] + eilReiksmes[3];
                Double vntKaina = Double.parseDouble(eilReiksmes[4]);
                Preke preke = new Preke(prekesId, prekesPavadinimas, vntKaina, 0);
                duomenys.put(prekesId, preke);
            }
            eilute = br.readLine();
            Integer pardavimuKiekis = Integer.parseInt(eilute);
            for (int i = 0; i < pardavimuKiekis; i++) {
                eilute = br.readLine();
                eilReiksmes = eilute.split(" ");
                Integer prekesId = Integer.parseInt(eilReiksmes[0]);
                Integer parduotaVnt = Integer.parseInt(eilReiksmes[1]);
                if (duomenys.containsKey(prekesId)) {
                    Preke prekeObj = duomenys.get(prekesId);
                    prekeObj.setKiekParduota(prekeObj.getKiekParduota() + parduotaVnt);
                }
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return duomenys;
    }

    public static Preke rastiPopuliariausia(Map<Integer, Preke> duomenys) {
        Preke populiariausia = null;
        Integer max = 0;

        for(Preke obj: duomenys.values()) {
            if (max < obj.getKiekParduota()) {
               max = obj.getKiekParduota();
               populiariausia = obj;
            }
        }

        return populiariausia;
    }

}
