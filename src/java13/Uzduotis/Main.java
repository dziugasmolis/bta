package java13.Uzduotis;

import java13.data.Employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Kazys", 1400.0, "administration"));
        list.add(new Employee("Jonas", 800.0, "store"));
        list.add(new Employee("Ona", 900.0, "sales"));
        list.add(new Employee("Petras", 1200.0, "sales"));
        list.add(new Employee("AAAAAA", 1200.0, "sales"));
        list.add(new Employee("Ada", 1500.0, "administration"));

        System.out.println(list.stream()
                .sorted((Comparator.comparing(Employee::getName))).collect(Collectors.toList()));
    }
}
