package java12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mainas {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(Spalvos.values()));
//
//        Spalvos[] masyvas = Spalvos.values();
//        for (int i = 0; i < masyvas.length; i++) {
//            System.out.println(masyvas[i]);
//        }

        Object kintamasis;
        kintamasis = 100;
        kintamasis = "aaaa";
        kintamasis = 100d;
        kintamasis = 'c';

        Saugykla obj = new Saugykla();
        obj.set(100);
        System.out.println((int)obj.get()+(int)obj.get());

        SaugyklaGeneric<Integer> objSkaicius = new SaugyklaGeneric<>();
        objSkaicius.set(100);
        System.out.println(objSkaicius.get() + objSkaicius.get());

        Number skaicius = 100;

        List<Double> raides = new ArrayList<>();
        suma(raides);

        Integer[] skaiciai = {1,2,3,4,5,6};
        String[] zodziai = {"a","b","c"};
        spausdinti(skaiciai);
        spausdinti(zodziai);
    }

    public static double suma(List<? extends Number> list) {
        double s = 0.0;
        for (Number n : list)
            s += n.doubleValue();
        return s;
    }

    public static <T> void spausdinti(T[] masyvas) {
        for (T obj: masyvas) {
            System.out.println(obj);
        }
    }
}
