package java17.Butai;

public class Butas {
    private Integer butoNr;
    private String adresas;
    private Integer kambariuSk;
    private Double plotas;
    private Integer nuomosKaina;

    public Butas(Integer butoNr, String adresas, Integer kambariuSk, Double plotas, Integer nuomosKaina) {
        this.butoNr = butoNr;
        this.adresas = adresas;
        this.kambariuSk = kambariuSk;
        this.plotas = plotas;
        this.nuomosKaina = nuomosKaina;
    }

    @Override
    public String toString() {
        return "Butas{" +
                "butoNr=" + butoNr +
                ", adresas='" + adresas + '\'' +
                ", kambariuSk=" + kambariuSk +
                ", plotas=" + plotas +
                ", nuomosKaina=" + nuomosKaina +
                '}';
    }

    public Integer getButoNr() {
        return butoNr;
    }

    public void setButoNr(Integer butoNr) {
        this.butoNr = butoNr;
    }

    public String getAdresas() {
        return adresas;
    }

    public void setAdresas(String adresas) {
        this.adresas = adresas;
    }

    public Integer getKambariuSk() {
        return kambariuSk;
    }

    public void setKambariuSk(Integer kambariuSk) {
        this.kambariuSk = kambariuSk;
    }

    public Double getPlotas() {
        return plotas;
    }

    public void setPlotas(Double plotas) {
        this.plotas = plotas;
    }

    public Integer getNuomosKaina() {
        return nuomosKaina;
    }

    public void setNuomosKaina(Integer nuomosKaina) {
        this.nuomosKaina = nuomosKaina;
    }
}
