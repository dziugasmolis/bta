package java17.Butai;

public class Kriterijai {
    private Integer kambariuSkNuo;
    private Integer kambariuSkIki;

    private Integer plotasNuo;
    private Integer plotasIki;

    private Integer kainaNuo;
    private Integer kainaIki;

    public Kriterijai(Integer kambariuSkNuo, Integer kambariuSkIki, Integer plotasNuo, Integer plotasIki, Integer kainaNuo, Integer kainaIki) {
        this.kambariuSkNuo = kambariuSkNuo;
        this.kambariuSkIki = kambariuSkIki;
        this.plotasNuo = plotasNuo;
        this.plotasIki = plotasIki;
        this.kainaNuo = kainaNuo;
        this.kainaIki = kainaIki;
    }

    public Integer getKambariuSkNuo() {
        return kambariuSkNuo;
    }

    public void setKambariuSkNuo(Integer kambariuSkNuo) {
        this.kambariuSkNuo = kambariuSkNuo;
    }

    public Integer getKambariuSkIki() {
        return kambariuSkIki;
    }

    public void setKambariuSkIki(Integer kambariuSkIki) {
        this.kambariuSkIki = kambariuSkIki;
    }

    public Integer getPlotasNuo() {
        return plotasNuo;
    }

    public void setPlotasNuo(Integer plotasNuo) {
        this.plotasNuo = plotasNuo;
    }

    public Integer getPlotasIki() {
        return plotasIki;
    }

    public void setPlotasIki(Integer plotasIki) {
        this.plotasIki = plotasIki;
    }

    public Integer getKainaNuo() {
        return kainaNuo;
    }

    public void setKainaNuo(Integer kainaNuo) {
        this.kainaNuo = kainaNuo;
    }

    public Integer getKainaIki() {
        return kainaIki;
    }

    public void setKainaIki(Integer kainaIki) {
        this.kainaIki = kainaIki;
    }
}
