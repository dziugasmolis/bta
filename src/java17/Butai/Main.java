package java17.Butai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String butuFailas = new File("").getAbsolutePath()
                + "/src/java17/Butai/Duomenys.txt";
        String kriterijuFailas = new File("").getAbsolutePath()
                + "/src/java17/Butai/Kriterijai.txt";
        List<Butas> butai = skaitytiButus(butuFailas);
        Kriterijai kriterijai = skaitytiKriterijus(kriterijuFailas);
        System.out.println(butai);
        System.out.println(kriterijai);

        List<Butas> atrinktiButai = atrinktiTinkancius(butai, kriterijai);
        System.out.println(atrinktiButai);
    }

    public static List<Butas> skaitytiButus(String failas) {
        List<Butas> objektuMasyvas = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(failas))) {
            String line = br.readLine();
            Integer eilKiekis = Integer.parseInt(line);
            line = br.readLine();
            for(int i = 0; i < eilKiekis; i++) {
                String[] objektai = line.split(" ");
                Integer butoNr = Integer.parseInt(objektai[0]);
                String gatve = objektai[1] + objektai[2] + objektai[3];
                Integer kambariuSk = Integer.parseInt(objektai[4]);
                Double kvadratura = Double.parseDouble(objektai[5]);
                Integer kaina = Integer.parseInt(objektai[6]);
                objektuMasyvas.add(new Butas(butoNr, gatve,
                        kambariuSk, kvadratura, kaina));
                line = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return objektuMasyvas;
    }

    public static Kriterijai skaitytiKriterijus(String failas) {
        Kriterijai obj = null;
        try (BufferedReader br = new BufferedReader(new FileReader(failas))) {
            String line = br.readLine();
            String[] objektas = line.split(" ");
            Integer minKambariuSk = Integer.parseInt(objektas[0]);
            Integer maxKambariuSk = Integer.parseInt(objektas[1]);

            line = br.readLine();
            objektas = line.split(" ");
            Integer minPlotas = Integer.parseInt(objektas[0]);
            Integer maxPlotas = Integer.parseInt(objektas[1]);

            line = br.readLine();
            objektas = line.split(" ");
            Integer minKaina = Integer.parseInt(objektas[0]);
            Integer maxKaina = Integer.parseInt(objektas[1]);

            obj = new Kriterijai(minKambariuSk, maxKambariuSk,
                    minPlotas, maxPlotas, minKaina, maxKaina);
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return obj;
    }

    public static List<Butas> atrinktiTinkancius(List<Butas> butai, Kriterijai kriterijai) {
        List<Butas> tinkantys = new ArrayList<>();
        for(Butas butas: butai) {
            if(butas.getKambariuSk() >= kriterijai.getKambariuSkNuo() &&
            butas.getKambariuSk() <= kriterijai.getKambariuSkIki() &&
            butas.getPlotas() >= kriterijai.getPlotasNuo() &&
            butas.getPlotas() <= kriterijai.getPlotasIki() &&
            butas.getNuomosKaina() >= kriterijai.getKainaNuo() &&
            butas.getNuomosKaina() <= kriterijai.getKainaIki()) {
                tinkantys.add(butas);
            }
        }
        return tinkantys;
    }
}
