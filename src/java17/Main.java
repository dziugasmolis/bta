package java17;

import java15.darbuotojai.Employee;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String duomenuFailoKelias = new File("").getAbsolutePath() +
                "/src/java17/Studentai.txt";
        Map<String, List<Studentas>> studentai = skaitymas(duomenuFailoKelias);
        System.out.println(studentai);
    }

    public static Map<String, List<Studentas>> skaitymas(String file) {
        Map<String, List<Studentas>> duomenys = new HashMap<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine();
            while(eilute != null) {
                String[] eilutesDuom = eilute.split(" ");
                Studentas stud = new Studentas(eilutesDuom[0], eilutesDuom[1], eilutesDuom[2]);
                if (duomenys.containsKey(stud.getGrupe())) {
                    List<Studentas> studentai = duomenys.get(stud.getGrupe());
                    studentai.add(stud);
                } else {
                    List<Studentas> list = new ArrayList<>();
                    list.add(stud);
                    duomenys.put(stud.getGrupe(), list);
                }
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return duomenys; // grazinam sukurta objekta
    }
}
