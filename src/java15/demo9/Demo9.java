package java15.demo9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Demo9 {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9}));

        List<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9}));
        System.out.println("ArrayListas");
        spausdintiList(arrayList);
        System.out.println("LinkedListas");
        spausdintiList(linkedList);

//        System.out.println(linkedList.peek());
//        System.out.println(linkedList.pop());
//        System.out.println(linkedList.pop());
//        System.out.println(linkedList);

        List<String> zodziai = new ArrayList<>(Arrays.asList(new String[]{"a", "b"}));

    }

    public static void spausdintiList(List<Integer> list) {
        for (Integer linkedL : list) {
            System.out.println(linkedL);
        }
    }
}
