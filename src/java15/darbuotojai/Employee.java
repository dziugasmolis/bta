package java15.darbuotojai;

public class Employee implements Comparable<Employee>{
    private String vardas;
    private String pavarde;
    private String departamentas;
    private Double alga;


    public Employee(String vardas, String pavarde, String departamentas, Double alga) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.departamentas = departamentas;
        this.alga = alga;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", departamentas='" + departamentas + '\'' +
                ", alga=" + alga +
                '}' + "\n";
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public String getDepartamentas() {
        return departamentas;
    }

    public void setDepartamentas(String departamentas) {
        this.departamentas = departamentas;
    }

    public Double getAlga() {
        return alga;
    }

    public void setAlga(Double alga) {
        this.alga = alga;
    }

    @Override
    public int compareTo(Employee kitaKlase) {
        int comp = getVardas().compareTo(kitaKlase.getVardas());
        if (comp != 0) return comp;
        comp = getPavarde().compareTo(kitaKlase.getPavarde());
        if (comp != 0) return comp;

        comp = getDepartamentas().compareTo(kitaKlase.getDepartamentas());
        if (comp != 0) return comp;

        return getAlga().compareTo(kitaKlase.getAlga());
    }

    @Override
    public boolean equals(Object o) {
        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }
        /* Check if o is an instance of Employee or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Employee)) {
            return false;
        }
        // typecast o to Employee so that we can compare data members
        Employee c = (Employee) o;

        // Compare the data members and return accordingly
        return getVardas().equals(c.getVardas()) &&
                getPavarde().equals(c.getPavarde()) &&
                getDepartamentas().equals(c.getDepartamentas()) &&
                getAlga().equals(c.getAlga());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getAlga().intValue();
        return result;
    }
}
