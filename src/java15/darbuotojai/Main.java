package java15.darbuotojai;

import java10.Keliautojai.Keliautojai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String duomenuFailoKelias = new File("").getAbsolutePath() +
                "/src/java15/darbuotojai/Duomenys.txt";
        List<Employee> darbuotojai = skaitymas(duomenuFailoKelias);
        Collections.sort(darbuotojai);
        System.out.println(darbuotojai);

        List<Employee> turtingieji = uzdirbaDaugiauNei1000(darbuotojai);
        System.out.println("Turtingieji: ");
        System.out.println(turtingieji);

        Map<String, Integer> kiekiai = new HashMap<>();
        System.out.println(kiekiai);
        kiekSkirtinguSkyriu(darbuotojai, kiekiai);
        System.out.println(kiekiai);

        Map<Employee, Integer> vienodi = rastiPasikartojancius(darbuotojai);
        System.out.println(vienodi);
    }

    public static Map<Employee, Integer> rastiPasikartojancius(List<Employee> darbuotojai) {
        Map<Employee, Integer> vienodi = new HashMap<>();

        for (int i = 0; i < darbuotojai.size(); i++) {
            for (int j = i + 1; j < darbuotojai.size(); j++) {
                if (darbuotojai.get(i).equals(darbuotojai.get(j))) {
                    if(vienodi.containsKey(darbuotojai.get(i))) {
                        Integer kiekisDarbuotoju = vienodi.get(darbuotojai.get(i));
                        vienodi.put(darbuotojai.get(i), ++kiekisDarbuotoju);
                    } else {
                        vienodi.put(darbuotojai.get(i), 2);
                    }
                    break;
                }
            }
        }

        return vienodi;
    }

    public static List<Employee> uzdirbaDaugiauNei1000(List<Employee> darbuotojai) {
        List<Employee> turtingieji = new ArrayList<>();
        for (Employee darbuotojas : darbuotojai) {
            if (darbuotojas.getAlga() > 1000) {
                turtingieji.add(darbuotojas);
            }
        }
        return turtingieji;
    }

    public static void kiekSkirtinguSkyriu(List<Employee> darbuotojai
            , Map<String, Integer> kiekiai) {
        for (int i = 0; i < darbuotojai.size(); i++) {
            if (kiekiai.containsKey(darbuotojai.get(i).getDepartamentas())) {
                Integer kiekis = kiekiai.get(darbuotojai.get(i).getDepartamentas());
                kiekiai.put(darbuotojai.get(i).getDepartamentas(), ++kiekis);
            } else {
                kiekiai.put(darbuotojai.get(i).getDepartamentas(), 1);
            }
        }
    }

    public static List<Employee> skaitymas(String file) {
        List<Employee> list = new ArrayList<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                String[] reiksmes = eilute.split(" ");
                list.add(new Employee(reiksmes[0], reiksmes[1],
                        reiksmes[2], Double.parseDouble(reiksmes[3])));
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return list; // grazinam sukurta objekta
    }
}
